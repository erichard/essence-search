<?php declare(strict_types=1);

namespace App\Repository;

use App\Search\GasPriceIndex;
use Carbon\Carbon;
use Elastic\Elasticsearch\ClientInterface;
use Elasticsearch\Common\Exceptions\Missing404Exception;

class GasPriceRepository
{
    public const ACTIVITE_PAR_PAGE = 12;

    public function __construct(
        private ClientInterface $client,
        private GasPriceIndex $gasPriceIndex
    ) {}

    public function findOneById(string $id): ?array
    {
        try {
            $response = $this->client->get([
                'index' => $this->gasPriceIndex::ALIAS,
                'id' => $id,
            ]);
        } catch (Missing404Exception $e) {
            return null;
        }

        $activite = $response['_source'];
        $activite['id'] = $response['_id'];

        return $activite;
    }

    public function findBySiren(string $siren): array
    {
        $response = $this->client->search([
            'index' => $this->gasPriceIndex::ALIAS,
            'body' => [
                'query' => [
                    'match' => [
                        'siren' => $siren,
                    ],
                ],
            ],
        ]);

        $activites = [];

        foreach ($response['hits']['hits'] as $activite) {
            $activites[] = array_merge($activite['_source'], ['id' => $activite['_id']]);
        }

        return $activites;
    }

    public function search(array $filters = []): array
    {
        $page = isset($filters['page']) ? (int) $filters['page'] : 1;

        if ($page < 1 || $page > 100) {
            $page = 1;
        }

        $params = [
            'index' => $this->gasPriceIndex::ALIAS,
            'body' => [
                '_source' => ['ville','prix','type', 'updated_at'],
                'aggs' => [
                    'count' => [
                        'value_count' => ['field' => 'id'],
                    ],
                    'types' => [
                        'terms' => ['field' => 'type']
                    ],
                    'regions' => [
                        'terms' => [
                            'field' => 'region',
                            'size' => 100,
                            'order' => ['_key' => 'asc']
                        ]
                    ],
                    'departements' => [
                        'terms' => [
                            'field' => 'departement',
                            'size' => 100,
                            'order' => ['_key' => 'asc']
                        ],

                    ],
                    'villes' => [
                        'terms' => [
                            'field' => 'ville',
                        ]
                    ],
                    'prix' => [
                        "percentiles" => [
                            "field" => "prix" 
                        ]
                    ]
                ],
                'size' => self::ACTIVITE_PAR_PAGE,
                'from' => ($page - 1) * self::ACTIVITE_PAR_PAGE,
                'query' => $this->getFilters($filters),
            ],
        ];


        if (isset($filters['distance']) && isset($filters['position'])) {
            $params['body']["script_fields"] = [
                "distance" => [
                    "script" => "doc['gps'].arcDistance(".$filters['position'].") / 1000"
                ]
            ];
        }

        $response = $this->client->search($params);

        $prices = [];
        $availableFilters = [
            'types' => array_column($response['aggregations']['types']['buckets'], 'doc_count', 'key'),
            'regions' => array_column($response['aggregations']['regions']['buckets'], 'doc_count', 'key'),
            'departements' => array_column($response['aggregations']['departements']['buckets'], 'doc_count', 'key'),
            'villes' => array_column($response['aggregations']['villes']['buckets'], 'doc_count', 'key'),
            'prix' => $response['aggregations']['prix']['values'],
        ];

        $prices = [];

        foreach ($response['hits']['hits'] as $result) {
            $item = $result['_source'];
            $item['updated_at'] = (new Carbon($item['updated_at']))->locale('fr_FR')->diffForHumans();

            if (isset($result['fields']) && isset($result['fields']['distance'])) {
                $item['distance'] = $result['fields']['distance'][0];
            }

            $prices[] = $item;
        }

        return [
            'total_results' => (int) $response['aggregations']['count']['value'],
            'prices' => $prices,
            'availableFilters' => $availableFilters,
        ];
    }

    public function getMarkers(array $filters = []): array
    {
        $result = $this->client->openPointInTime([
            'index' => $this->gasPriceIndex::ALIAS,
            'keep_alive' => '1m',
        ]);
        $pit = $result['id'];

        $params = [
            'body' => [
                'pit' => [
                    'id' => $pit,
                    'keep_alive' => '1m',
                ],
                '_source' => ['gps'],
                'size' => 100,
                'track_total_hits' => false,
                'sort' => ['_shard_doc' => 'desc'],
                'query' => $this->getFilters($filters),
            ],
        ];

        $response = $this->client->search($params);

        $markers = [];

        while (100 === count($response['hits']['hits'])) {
            foreach ($response['hits']['hits'] as $activite) {
                $markers[] = array_merge($activite['_source'], ['id' => $activite['_id']]);
                $searchAfter = $activite['sort'];
            }

            $params['body']['search_after'] = $searchAfter;
            $response = $this->client->search($params);
        }

        foreach ($response['hits']['hits'] as $activite) {
            $markers[] = array_merge($activite['_source'], ['id' => $activite['_id']]);
            $searchAfter = $activite['sort'];
        }

        return $markers;
    }

    private function getFilters(array $filters = []): array
    {
        $boolFilter = [
            "bool" => [
                "must" => [
                    [ "match_all" => (object)[] ]
                ],
                "filter" => []
            ]
        ];

        $query = [
            'function_score' => [
                "query" => [],
                //"score_mode" => 'sum',
                "functions"=> [
                    [
                        "gauss" => [
                            'updated_at' => [
                                'scale' => '3d'
                            ],
                        ],
                        //"weight" => 0.8
                    ],
                    [
                        "exp" => [
                            'prix' => [
                                'origin' => '0',
                                'scale' => '0.05'
                            ],
                        ],
                        //"weight" => 1
                    ],

                ]
            ]
        ];

        if (isset($filters['type'])) {
            $boolFilter['bool']['filter'][] = ['terms' => ['type' => $filters['type']]];
        }

        if (isset($filters['region'])) {
            $boolFilter['bool']['filter'][] = ['terms' => ['region' => $filters['region']]];
        }

        if (isset($filters['departement'])) {
            $boolFilter['bool']['filter'][] = ['terms' => ['departement' => $filters['departement']]];
        }

        if (isset($filters['ville'])) {
            $boolFilter['bool']['filter'][] = ['terms' => ['ville' => $filters['ville']]];
        }

        if (isset($filters['distance']) && isset($filters['position'])) {
            $boolFilter['bool']['filter'][] = ['geo_distance' => [
                'distance' => $filters['distance'].'km',
                'gps' => $filters['position']
            ]];
            $query['function_score']["functions"][] = [
                "gauss" => [
                    "gps"=> [
                      "origin"=> $filters['position'],
                      "scale"=>  "10km",
                    ],
                ],
                //"weight" => 0.8
            ];
        }

        $query['function_score']['query'] = $boolFilter;

        return $query;
    }

    public function autocomplete(string $query): array
    {
        $boolQuery = $this->getFilters();

        $params = [
            'body' => [
                'size' => 0,
                'track_total_hits' => false,
                'query' => $boolQuery,
                'suggest' => [
                    'ville' => [
                        'prefix' => $query,
                        'completion' => [
                            'field' => 'ville.autocomplete',
                            'skip_duplicates' => true,
                        ],
                    ],
                ],
            ],
        ];

        $response = $this->client->search($params);

        $results = [
            'villes' => array_column($response['suggest']['ville'][0]['options'], 'text'),
        ];

        return $results;
    }
}
