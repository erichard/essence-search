<?php declare(strict_types=1);

namespace App\Command;

use App\Search\GasPriceIndex;
use App\Search\Indexer;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:index-gas-price',
    description: 'Index les prix des carburants dans le moteur de recherche',
)]
class IndexProductCommand extends Command
{
    public function __construct(
        private Indexer $indexer,
        private GasPriceIndex $productIndex,
        public iterable $populators
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addOption('reindex', null, InputOption::VALUE_NONE, "Utilise l'index précédent au lieu de la base de donnée. Bien plus rapide !");
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $indexName = $this->indexer->createIndex($this->productIndex->mapping());

        if ($input->getOption('reindex')) {
            $currentIndex = $this->indexer->getCurrentIndex($this->productIndex::ALIAS);
            $this->indexer->reindex($currentIndex, $indexName);
        } else {
            foreach ($this->populators as $populator) {
                $this->indexer->populate($indexName, $populator);
            }
        }

        $this->indexer->makeLive($this->productIndex::ALIAS, $indexName);
        $this->indexer->cleanup($this->productIndex::ALIAS);

        return Command::SUCCESS;
    }
}
