<?php declare(strict_types=1);

namespace App\Controller;

use App\Repository\GasPriceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    public function __construct(
        private GasPriceRepository $gasPriceRepository
    ) {}

    #[Route('/', name: 'app_home')]
    public function index(Request $request): Response
    {
        $filters = $request->query->all();
        $filters = array_filter($filters);

        $results = $this->gasPriceRepository->search($filters);


        foreach($results['prices'] as &$price) {
            $class = '';

            if ($price["prix"] <= $results['availableFilters']['prix']['5.0']) {
                $class = 'text-success';
            }
            if ($price["prix"] >= $results['availableFilters']['prix']['95.0']) {
                $class = 'text-warning';
            }

            $price['class'] = $class;
        }

        return $this->render('home/index.html.twig', [
            'results' => $results['prices'],
            'availableFilters' => $results['availableFilters'],
            'filters' => $filters,
            'count' => $results['total_results'],
        ]);
    }

    #[Route('/search', name: 'search')]
    public function search(Request $request): Response
    {
        $filters = $request->query->all();
        $filters = array_filter($filters);

        $results = $this->gasPriceRepository->search($filters);

        return $this->json($results);
    }

    #[Route('/markers', name: 'markers')]
    public function markers(Request $request): Response
    {
        $filters = $request->query->all();
        $filters = array_filter($filters);

        $markers = $this->gasPriceRepository->getMarkers($filters);

        return $this->json($markers);
    }

    #[Route('/markers/{id}', name: 'marker_info')]
    public function marker(string $id): Response
    {
        $activite = $this->gasPriceRepository->findOneById($id);

        return $this->json($activite);
    }

    #[Route('/autocomplete', name: 'autocomplete')]
    public function autocomplete(Request $request): Response
    {
        $results = $this->gasPriceRepository->autocomplete($request->query->get('q'));

        return $this->json($results);
    }
}
