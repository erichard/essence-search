<?php declare(strict_types=1);

namespace App\Search;

use League\Csv\Reader;

class GasPricePopulator implements ProductPopulatorInterface
{
    public function __construct(
        private string $gasFile
    ) {}

    public function getItems(): iterable
    {
        $csv = Reader::createFromPath($this->gasFile, 'r');
        $csv->setDelimiter(';');
        $csv->setHeaderOffset(0);

        return $csv;
    }

    public function getIdentifierValue($item)
    {
        return $item['id'].'_'.$item['prix_id'];
    }

    public function isIndexable($item): bool
    {
        return !empty($item['prix']);
    }

    public function transform($item): array
    {
        list($lat, $lng) = explode(',', $item['geom']);

        return [
            'id' => $item['id'],
            'ville' => trim($item['com_name']),
            'departement' => trim($item['dep_name']),
            'region' => trim($item['reg_name']),
            'updated_at' => $item['prix_maj'],
            'gps' => [
                'lat' => $lat,
                'lon' => $lng,
            ],
            'prix' => $item['prix_valeur'],
            'prix_id' => $item['prix_id'],
            'type' => trim($item['prix_nom']),
            'services' => explode('//', $item['services_service']),
        ];
    }

    public function getBatchSize(): int
    {
        return 100;
    }
}
