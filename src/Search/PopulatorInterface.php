<?php declare(strict_types=1);

namespace App\Search;

interface PopulatorInterface
{
    public function getItems(): iterable;

    public function getIdentifierValue($item);

    public function isIndexable($item): bool;

    public function transform($item): array;

    public function getBatchSize(): int;
}
