<?php declare(strict_types=1);

namespace App\Search;

use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Psr\Log\LoggerInterface;

class Indexer
{
    public function __construct(
        public ClientInterface $elasticClient,
        public LoggerInterface $logger,
    ) {}

    public function createIndex(array $mapping): string
    {
        $result = $this
            ->elasticClient
            ->indices()
            ->create($mapping)
            ->asArray();

        return $result['index'];
    }

    public function populate(string $index, PopulatorInterface $populator)
    {
        $payload = [
            'index' => $index,
            'body' => [],
        ];

        $items = $populator->getItems();

        $i = 0;
        foreach ($items as $item) {
            $identifier = $populator->getIdentifierValue($item);
            $document = $populator->transform($item);

            if (!$populator->isIndexable($document)) {
                $this->logger->debug("Document #$identifier is not valid for indexation");
                continue;
            }

            // Push the action and the source in the bulk payload
            $payload['body'][] = ['index' => [
                '_id' => $identifier,
            ]];
            $payload['body'][] = $document;

            ++$i;

            if (0 === $i % $populator->getBatchSize()) {
                $this->flush($payload);
                $payload['body'] = [];
            }
        }

        if (!empty($payload['body'])) {
            $this->flush($payload);
        }

        $this
            ->elasticClient
            ->indices()
            ->refresh(['index' => $index]);
    }

    public function makeLive(string $alias, string $index)
    {
        $currentIndex = $this->getCurrentIndex($alias);

        if (null !== $currentIndex) {
            $this
                ->elasticClient
                ->indices()
                ->deleteAlias(['name' => $alias, 'index' => $currentIndex]);
        }

        $this
            ->elasticClient
            ->indices()
            ->putAlias(['name' => $alias, 'index' => $index]);
    }

    public function reindex(string $sourceIndex, string $destIndex)
    {
        $this->elasticClient->reindex([
            'refresh' => true,
            'body' => [
                'source' => ['index' => $sourceIndex],
                'dest' => ['index' => $destIndex],
            ],
        ]);
    }

    public function cleanup(string $alias)
    {
        $indices = array_keys($this
            ->elasticClient
            ->indices()
            ->get(['index' => $alias.'_*'])
            ->asArray());

        $currentIndex = $this->getCurrentIndex($alias);

        if (null !== $currentIndex && false !== $i = array_search($currentIndex, $indices, true)) {
            unset($indices[$i]);
        }

        if (!empty($indices)) {
            $this
                ->elasticClient
                ->indices()
                ->delete(['index' => $indices]);
        }
    }

    private function flush($payload)
    {
        $responses = $this->elasticClient->bulk($payload)->asArray();

        if ($responses['errors']) {
            $errors = array_filter($responses['items'], function ($item) {
                return !in_array($item['index']['status'], [201, 200], true);
            });

            foreach ($errors as $error) {
                $message = $error['index']['error']['reason'];
                if (isset($error['index']['error']['caused_by'])) {
                    $message .= ' '.$error['index']['error']['caused_by']['reason'];
                }

                $this->logger->error($message);
            }

            throw new \RuntimeException('Errors during populate. Check the log for details.');
        }

        unset($responses);
    }

    public function getCurrentIndex(string $alias)
    {
        try {
            $alias = $this
                ->elasticClient
                ->indices()
                ->getAlias(['name' => $alias])
                ->asArray();

            return key($alias);
        } catch (ClientResponseException $e) {
            return null;
        }
    }
}
