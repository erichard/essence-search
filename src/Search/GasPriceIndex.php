<?php declare(strict_types=1);

namespace App\Search;

class GasPriceIndex
{
    public const ALIAS = 'gas_price';

    public function mapping(): array
    {
        $date = date('YmdHis');

        $name = 'gas_price_'.$date;

        return [
            'index' => $name,
            'body' => [
                'settings' => [
                    'number_of_shards' => 1,
                    'number_of_replicas' => 0,
                    'refresh_interval' => -1,
                ],
                'mappings' => [
                    'properties' => [
                        'id' => [
                            'type' => 'keyword',
                        ],
                        'region' => [
                            'type' => 'keyword',
                        ],
                        'services' => [
                            'type' => 'text',
                            'analyzer' => 'french',
                            'fields' => [
                                'autocomplete' => [
                                    'type' => 'completion'
                                ],
                                'raw' => [
                                    'type' => 'keyword'
                                ],
                            ]
                        ],
                        'departement' => [
                            'type' => 'keyword',
                        ],
                        'ville' => [
                            'type' => 'keyword',
                            'fields' => [
                                'autocomplete' => [
                                    'type' => 'completion'
                                ],
                            ]
                        ],
                        'gps' => [
                            'type' => 'geo_point',
                        ],
                        'prix' => [
                            'type' => 'float',
                        ],
                        'type' => [
                            'type' => 'keyword',
                        ],
                        'updated_at' => [
                            'type' => 'date',
                        ]
                    ],
                ],
            ],
        ];
    }
}
