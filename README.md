# Des recherches efficaces et pertinentes avec ElasticSearch

La présentation en PDF est disponible [ici](public/talk.pdf).

Mon queryBuilder est accessible ici : https://github.com/erichard/elasticsearch-query-builder 