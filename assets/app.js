/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

// Vue
import leaflet from "leaflet";
import "leaflet.markercluster";
import "leaflet-spin";

import Alpine from 'alpinejs'
window.Alpine = Alpine
window.Alpine.start()

let mapInstance = null;
let markerClusterGroupInstance = null;

if (!mapInstance) {
  const urlParams = new URLSearchParams(window.location.search);
  let center = [47.824905, 2.618787];
  if (urlParams.get('position')) {
    center = urlParams.get('position').split(',');
  } 
  mapInstance = leaflet.map("map").setView(center, 6);
}

// afficher un loader le temps que les marqueurs se chargent.
mapInstance.spin(true);

// Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
leaflet
  .tileLayer(
    "https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}.png",
    {
      // Il est toujours bien de laisser le lien vers la source des données
      attribution:
        'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
      maxZoom: 19,
    }
  )
  .addTo(mapInstance);

mapInstance.on('click', function(event) {
  document.getElementById('position').value = event.latlng.lat + ',' + event.latlng.lng;
  document.getElementById('filtersForm').submit()
});

// add zoom / dezoom buttons
leaflet.control.scale().addTo(mapInstance);

if (!markerClusterGroupInstance) {
  markerClusterGroupInstance = leaflet.markerClusterGroup({
    chunkedLoading: true,
    showCoverageOnHover: true,
  });
}

// clear previous markers, if any:
markerClusterGroupInstance.clearLayers();

fetch('markers'+window.location.search)
  .then(data => data.json())
  .then(markers => {
    markers.forEach(m => {
      var myIcon = leaflet.icon({
        iconUrl: '/images/marker-icon.png',
        iconSize: [25, 41],
      });
      const marker = leaflet.marker(leaflet.latLng(m.gps.lat, m.gps.lon), {
        icon: myIcon,
        // On stocke l'id du marqueur pour ensuite chercher le contenu de la popup en interrogeant l'API
        _markerId: m.id,
      });
      marker.bindPopup("Loading...");
      // on click, load marker content from the api
      marker.on("click", (e) => {
        const popup = e.target.getPopup();
        const markerId = e.target.options._markerId;
        apiRequest("/markers/" + markerId)
          .then((r) => {
            const content = this.popupTemplate(r);
            popup.setContent(content);
          })
          .catch((e) => {
            popup.setContent(e);
            throw new Error(e);
          });
      });
      markerClusterGroupInstance.addLayer(marker);
    })

    mapInstance.spin(false);
  });

mapInstance.addLayer(markerClusterGroupInstance);
